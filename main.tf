resource "aws_security_group" "alb-sg" {
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    from_port   = var.alb_port
    to_port     = var.alb_port
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

locals {
  subnets       = ["subnet-a295a1c4", "subnet-af4e3ff5"]
  vpc           = "vpc-50897329"
}

resource "aws_security_group" "service-sg" {
  description = "allow inbound access from the ALB only"
  vpc_id      = local.vpc

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    from_port       = var.service_port
    to_port         = var.service_port
    protocol        = "tcp"
    security_groups = [aws_security_group.alb-sg.id]
  }
}

module "alb" {
  source  = "terraform-aws-modules/alb/aws"
  version = "5.6.0"

  name = "${var.name}-alb"

  vpc_id  = local.vpc
  subnets = local.subnets

  security_groups = [aws_security_group.alb-sg.id]

  target_groups = [
    {
      name_prefix      = "pref-"
      backend_protocol = "HTTP"
      backend_port     = var.service_port
      target_type      = "ip"
    }
  ]

  http_tcp_listeners = [
    {
      port               = var.alb_port
      protocol           = "HTTP"
      target_group_index = 0
    }
  ]
}

data "aws_caller_identity" "current" {}

module "ecs_container_definition" {
  source          = "cloudposse/ecs-container-definition/aws"
  version         = "0.37.0"
  container_image = "${data.aws_caller_identity.current.account_id}.dkr.ecr.${var.aws_region}.amazonaws.com/${var.container_image}"
  container_name  = var.name
  port_mappings = [
    {
      containerPort = var.service_port
      hostPort      = var.service_port
      protocol      = "tcp"
    }
  ]
  environment = var.environment
}

resource "aws_ecs_task_definition" "task-definition" {
  family                   = var.name
  network_mode             = "awsvpc"
  requires_compatibilities = ["FARGATE"]
  cpu                      = var.cpu
  memory                   = var.memory
  execution_role_arn       = "arn:aws:iam::${data.aws_caller_identity.current.account_id}:role/ecsTaskExecutionRole"
  container_definitions    = module.ecs_container_definition.json
}

resource "aws_ecs_service" "ecs-service" {
  name            = var.name
  desired_count   = 1
  cluster         = "arn:aws:ecs:${var.aws_region}:${data.aws_caller_identity.current.account_id}:cluster/${var.cluster_name}"
  task_definition = aws_ecs_task_definition.task-definition.arn
  launch_type     = "FARGATE"

  network_configuration {
    security_groups  = [aws_security_group.service-sg.id]
    subnets          = local.subnets
    assign_public_ip = true
  }

  load_balancer {
    target_group_arn = module.alb.target_group_arns[0]
    container_name   = var.name
    container_port   = var.service_port
  }

  depends_on = [
    module.alb,
  ]
}
