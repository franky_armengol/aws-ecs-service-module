# Terraform-aws-ecs-service-module

Terraform module to generate ecs service using fargate.

## Prerequisites

- [Terraform](https://www.terraform.io/)
- [Aws-cli](https://docs.aws.amazon.com/cli/latest/userguide/install-bundle.html)
- [Terraform-docs](https://github.com/segmentio/terraform-docs)

## Dependencies

This repo suposes that you have a vpc and ecs cluster created before.

## Modules

- [alb](https://github.com/terraform-aws-modules/terraform-aws-alb)(v5.6.0)
- [ecs-container-definition](https://github.com/cloudposse/terraform-aws-ecs-container-definition)(v0.37.0)

## Usage example

```terraform
module "service" {
  source          = "git@gitlab.com:franky_armengol/terraform-aws-ecs-service-module.git"
  service_port    = var.service_port
  name            = var.name
  container_image = var.container_image
  environment = [
    {
      name  = "PORT"
      value = var.service_port
    }
  ]
}
```

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:-----:|
| name | Name of the service. | `string` | | yes |
| container\_name | Name of the conainter image with his tag. | `string` | | yes |
| service\_port | Service internal port | `number` | | yes |
| aws\_region | Aws region to deploy all resources | `string` | `"eu-west-1"` | no |
| alb\_port | Alb external port | `number` | `80` | no |
| cluster\_name | Name of the cluster we want to deploy the service. | `string` | `testing` | no |


## Documentation

For generate inputs and outputs for documentation:

```bash
terraform-docs markdown .
```

## Providers

| Name | Version |
|------|---------|
| aws | 2.70.0 |
| local | 1.4.0 |
| terraform | >= 0.12.0 |

## Authors

Module is maintained by [Francesc Armengol](https://gitlab.com/franky_armengol).