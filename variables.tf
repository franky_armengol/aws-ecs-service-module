variable "aws_region" {
  description = "Aws region to deploy all resources"
  type        = string
  default     = "eu-west-1"
}

variable "service_port" {
  description = "Service internal port"
}

variable "name" {
  type        = string
  description = "The name of the container. Up to 255 characters ([a-z], [A-Z], [0-9], -, _ allowed)"
}

variable "container_image" {
  type        = string
  description = "The image used to start the container."
}

variable "alb_port" {
  description = "ALB external port"
  default     = 80
}

variable "cluster_name" {
  type        = string
  description = "Name of the cluster we want to deploy the service."
  default     = "testing"
}

variable "cpu" {
  description = "Cpu units for fargate compatibility. More info: https://docs.aws.amazon.com/AmazonECS/latest/developerguide/AWS_Fargate.html"
  default     = 256
}

variable "memory" {
  description = "Memory in MB for fargate compatibility. More info: https://docs.aws.amazon.com/AmazonECS/latest/developerguide/AWS_Fargate.html"
  default     = 512
}

variable "environment" {
  description = "The environment variables to pass to the container. This is a list of maps."
  type = list(object({
    name  = string
    value = string
  }))
}
